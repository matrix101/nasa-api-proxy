const express = require("express");
const axios = require("axios");
const slowDown = require("express-slow-down");
const router = express.Router();
const BASE_URL = "https://api.nasa.gov/insight_weather/?";

let cacheData;
let cacheTime;

const speedLimiter = slowDown({
  windowMs: 30 * 1000,
  delayAfter: 2,
  delayMs: 500 
});

const apiKeys = new Map();

apiKeys.set('nvkDjsiwW2', true);


router.get("/", speedLimiter ,async (req, res, next) => {
  const apiKey = req.get('X-API-KEY');
  if(apiKeys.has(apiKey))
    next();
  else 
    next(new Error("X-API-KEY Header is required"));
}, async (req, res, next) => {
  try {
    if (cacheTime && cacheTime > Date.now() - 30 * 1000) {
      return res.json(cacheData);
    }

    const params = new URLSearchParams({
      api_key: process.env.NASA_API_KEY,
      feedtype: "json",
      ver: 1.0,
    });

    const { data } = await axios.get(BASE_URL + params);

    cacheData = data;
    cacheTime = Date.now();
    data.cacheDuration = cacheTime;

    return res.json(data);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
